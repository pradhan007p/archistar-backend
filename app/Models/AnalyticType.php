<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AnalyticType extends Model
{
    protected $table = 'analytic_types';

    protected $fillable = [
        'name',
        'units',
        'is_numeric',
        'num_decimal_places'
    ];

    /**
     * @return HasMany
     */
    public function propertyAnalytics()
    {
        return $this->hasMany(PropertyAnalytics::class);
    }
}
