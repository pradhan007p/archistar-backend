<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use \Illuminate\Support\Str;

class Property extends Model
{
    /**
     * @var string
     */
    protected $table = 'properties';

    /** @var array */
    protected $fillable = ['guid', 'suburb', 'state', 'country'];

    /**
     * Generate Uuid and Set Guid Attribute
     */
    public function setGuidAttribute()
    {
        $this->attributes['guid'] = (string) Str::uuid();
    }

    /**
     * Format Created at date to Y-m-d
     *
     * @param $value
     * @return mixed
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d M Y');
    }

    /**
     * @return HasMany
     */
    public function propertyAnalytics()
    {
        return $this->hasMany(PropertyAnalytics::class);
    }
}
