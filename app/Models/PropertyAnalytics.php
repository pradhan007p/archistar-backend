<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PropertyAnalytics extends Model
{
    protected $table = 'property_analytics';

    /** @var array */
    protected $fillable = ['property_id', 'analytic_type_id', 'value'];

    /**
     * Create Relation to Property Table
     *
     * @return BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id', 'id')
            ->select(['id', 'suburb', 'state', 'country']);
    }

    /**
     * Create Relation to Analytics Type Table
     *
     * @return BelongsTo
     */
    public function analyticType()
    {
        return $this->belongsTo(AnalyticType::class, 'analytic_type_id', 'id')
            ->select(['id', 'name', 'units', 'is_numeric', 'num_decimal_places']);
    }
}
