<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyAnalyticsRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property_id'      => 'required|integer|unique:property_analytics,property_id,NULL,id,analytic_type_id,'.$this->analytic_type_id,
            'analytic_type_id' => 'required|integer|unique:property_analytics,analytic_type_id,NULL,id,property_id,'.$this->property_id,
            'value'            => 'required|string',
        ];
    }
}
