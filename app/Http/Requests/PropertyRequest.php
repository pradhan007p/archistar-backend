<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'suburb'  => 'required|string|max:150',
            'state'   => 'required|string|max:50',
            'country' => 'required|string|max:100',
        ];
    }
}
