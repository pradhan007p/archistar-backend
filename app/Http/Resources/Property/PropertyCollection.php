<?php

namespace App\Http\Resources\Property;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($item) {
                return [
                    'id'         => $item->id,
                    'suburb'     => $item->suburb,
                    'state'      => $item->state,
                    'country'    => $item->country,
                    'created_at' => $item->created_at,
                ];
            })
        ];
    }
}
