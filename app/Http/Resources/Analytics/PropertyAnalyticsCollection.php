<?php

namespace App\Http\Resources\Analytics;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyAnalyticsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($item) {
                return [
                    'id'               => $item->id,
                    'property_id'      => $item->property_id,
                    'analytic_type_id' => $item->analytic_type_id,
                    'value'            => $item->value,
                    'property'         => $item->property,
                    'analytic_type'    => $item->analyticType,
                ];
            })
        ];
    }
}
