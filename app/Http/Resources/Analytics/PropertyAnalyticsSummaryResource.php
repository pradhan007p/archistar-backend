<?php

namespace App\Http\Resources\Analytics;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyAnalyticsSummaryResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'max'     => $this->max,
            'min'     => $this->min,
            'avg'     => $this->avg,
            'suburb'  => $this->suburb,
            'state'   => $this->state,
            'country' => $this->country,
        ];
    }
}
