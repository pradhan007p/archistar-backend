<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PropertyAnalyticsRequest;
use App\Http\Resources\Analytics\PropertyAnalyticsCollection;
use App\Http\Resources\Analytics\PropertyAnalyticsResource;
use App\Http\Resources\Analytics\PropertyAnalyticsSummaryResource;
use App\Repositories\PropertyAnalyticRepository;
use Matrix\Exception;

class PropertyAnalyticsController extends ApiBaseController
{
    private $analyticsRepository;

    public function __construct(PropertyAnalyticRepository $analyticRepository)
    {
        $this->analyticsRepository = $analyticRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return PropertyAnalyticsCollection|\Illuminate\Http\JsonResponse
     */
    public function index($propertyId)
    {
        try {
            $response = $this->analyticsRepository->getPropertyAnalytics($propertyId);

            return (new PropertyAnalyticsCollection($response));

        } catch (Exception $e) {
            return $this->handleException($e);
        }
    }

    /**
     * Get Property Analytices based on provided input filters
     *
     * @param $type
     * @param $value
     *
     * @return PropertyAnalyticsSummaryResource|\Illuminate\Http\JsonResponse
     */
    public function propertyAnalyticsSummary($type, $value)
    {
        try {
            $response = $this->analyticsRepository->getAnalyticsSummary($type, $value);

            if ($response)
                return (new PropertyAnalyticsSummaryResource($response));
            else
                return $this->success('');

        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PropertyAnalyticsRequest  $request
     * @return PropertyAnalyticsResource|\Illuminate\Http\JsonResponse
     */
    public function store(PropertyAnalyticsRequest $request)
    {
        try {
            $data = $request->only(['property_id', 'analytic_type_id', 'value']);
            $response = $this->analyticsRepository->createAnalytics($data);

            return (new PropertyAnalyticsResource($response))
                ->additional(['status_code' => 201, 'message' => 'Analytics Created!']);

        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    /**
     * Update the specified analytics data
     *
     * @param  PropertyAnalyticsRequest  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PropertyAnalyticsRequest $request, $id)
    {
        try {
            $data = $request->only(['property_id', 'analytic_type_id', 'value']);
            $response = $this->analyticsRepository->updateAnalytics($id, $data);

            if ($response) {
                return $this->success('Analytics Updated!');
            } else {
                return $this->fail('Failed to update Analytics');
            }

        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }
}
