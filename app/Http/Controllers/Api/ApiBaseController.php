<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\JsonResponse as JsonResponseAlias;
use Illuminate\Support\Facades\Log;

class ApiBaseController extends Controller
{
    /**
     * @param $message
     * @param  array  $data
     * @param  int  $code
     *
     * @return JsonResponse
     */
    protected function success($message = null, $data = [], $code = 200)
    {
        return response()->json([
            'status'      => 'success',
            'status_code' => $code,
            'message'     => $message ? $message : 'OK',
            'data'        => $data
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_OK);
    }

    /**
     * Convert Laravel Exception to Readable format
     *
     * @param  Exception  $e
     * @return JsonResponseAlias
     */
    protected function handleException($e)
    {
        Log::error('ERROR', [$e->getTraceAsString()]);

        $message = env('APP_DEBUG') ? $e->getMessage() : 'Something went wrong, please try again!';
        return $this->fail($message);
    }

    /**
     * @param  string|null  $message
     * @param  int  $code
     * @param  array  $errors
     *
     * @return JsonResponse
     */
    protected function fail($message = null, $code = 500, $errors = [])
    {
        return response()->json([
            'status'      => 'failure',
            'status_code' => $code,
            'message'     => $message,
            'errors'      => $errors,
        ], isset(JsonResponse::$statusTexts[$code]) ? $code : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }
}
