<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyRequest;
use App\Http\Resources\Property\PropertyCollection;
use App\Http\Resources\Property\PropertyResource;
use App\Repositories\PropertyRepository;
use Illuminate\Http\Request;

class PropertyController extends ApiBaseController
{
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    /**
     * PropertyController constructor.
     * @param  PropertyRepository  $propertyRepository
     */
    public function __construct(PropertyRepository $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * Display a listing of all the property.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $response = $this->propertyRepository->listProperties();

            return (new PropertyCollection($response));

        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  PropertyRequest  $request
     * @return PropertyResource|\Illuminate\Http\JsonResponse
     */
    public function store(PropertyRequest $request)
    {
        try {
            $data = $request->only(['suburb', 'state', 'country']);
            $response = $this->propertyRepository->createProperty($data);

            return (new PropertyResource($response))
                ->additional(['status_code' => 201, 'message' => 'Property Created!']);

        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return PropertyResource|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $response = $this->propertyRepository->getPropertyDetails($id);

            return (new PropertyResource($response));

        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  PropertyRequest  $request
     * @param  int  $id
     * @return PropertyResource|\Illuminate\Http\JsonResponse
     */
    public function update(PropertyRequest $request, $id)
    {
        try {
            $data = $request->only(['suburb', 'state', 'country']);
            $response = $this->propertyRepository->updatePropertyDetails($id, $data);
            if ($response) {
                return $this->success('Property Details Updated!');
            } else {
                return $this->fail('Failed to update Property');
            }
        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }
}
