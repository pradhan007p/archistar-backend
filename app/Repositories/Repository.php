<?php


namespace App\Repositories;

/**
 * Class Repository
 * @package App\Repositories
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * Store assigned model from child classes
     *
     * @var $model
     */
    protected $model;

    public function __construct()
    {
        $this->model = $this->setModel();
    }

    /**
     * Abstract Function to set the specific model based on the Repository
     *
     * @return mixed
     */
    abstract function setModel();

    /**
     * Get all resources
     *
     * @param  array  $columns
     * @return Collection
     */
    public function all($columns = ['*'])
    {
        return $this->model
            ->all($columns);
    }


    /**
     * Get paginated resources with given limit
     *
     * @param $limit
     * @return Collection
     */
    public function paginate($limit = 15)
    {
        return $this->model
            ->paginate($limit);
    }

    /**
     * Store newly created resource
     *
     * @param  array  $data
     * @return mixed|Object
     */
    public function store(array $data)
    {
        return $this->model
            ->create($data);
    }

    /**
     * Update specific resource.
     *
     * @param  array  $data
     * @param $id
     * @return bool
     */
    public function update($id, array $data)
    {
        return $this->model
            ->find($id)
            ->update($data);
    }

    /**
     * Delete specific resource
     *
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        return $this->model
            ->destroy($id);
    }

    /**
     * Find specific resource
     *
     * @param $id
     * @param  array  $columns
     * @return Object
     */
    public function find($id, $columns = ['*'])
    {
        return $this->model
            ->find($id, $columns);
    }

    /**
     * Find specific resource by given attribute
     *
     * @param  array  $condition
     * @param  array  $columns
     * @return Object
     */
    public function findBy($condition, $columns = ['*'])
    {
        return $this->model
            ->where($condition)
            ->first($columns);
    }
}
