<?php

namespace App\Repositories;

use App\Models\Property;

class PropertyRepository extends Repository
{

    /**
     * Set Model to the $model object in parent class
     * Returns Model Object
     *
     * @return mixed
     */
    function setModel()
    {
        return (new Property());
    }


    /**
     * List properties
     *
     * @param  int  $limit
     * @return Collection
     */
    public function listProperties($limit = 10)
    {
        return $this->paginate($limit);
    }

    /**
     * Add New Property
     *
     * @param $data
     * @return mixed|Object
     */
    public function createProperty($data)
    {
        $data['guid'] = '';
        return $this->store($data);
    }

    /**
     * Get the details of specific property
     *
     * @param $id
     * @return Object
     */
    public function getPropertyDetails($id)
    {
        $result = $this->find($id);
        if (empty($result))
            abort(404, 'Property not found for given ID');

        return $result;
    }

    /**
     * Update Specific Property Details
     *
     * @param $id
     * @param $data
     * @return bool
     */
    public function updatePropertyDetails($id, $data)
    {
        return $this->update($id, $data);
    }
}
