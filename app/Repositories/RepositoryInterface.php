<?php


namespace App\Repositories;

/**
 * Interface RepositoryInterface
 *
 * @package App\Repositories
 */
interface RepositoryInterface
{
    /**
     * Get all resources
     *
     * @param  array  $columns
     * @return mixed
     */
    function all($columns = ['*']);

    /**
     * Get paginated resources with given limit
     *
     * @param $limit
     * @return mixed
     */
    function paginate($limit = 10);

    /**
     * Store newly created resource
     *
     * @param  array  $data
     * @return Object
     */
    function store(array $data);

    /**
     * Update specific resource.
     *
     * @param  array  $data
     * @param $id
     * @return bool
     */
    function update($id, array $data);

    /**
     * Delete specific resource
     *
     * @param $id
     * @return bool
     */
    function delete($id);

    /**
     * Find specific resource
     *
     * @param $id
     * @param  array  $columns
     * @return Object
     */
    function find($id, $columns = ['*']);

    /**
     * Find specific resource by given attribute
     *
     * @param  array  $condition
     * @param  array  $columns
     * @return Object
     */
    public function findBy($condition, $columns = ['*']);
}
