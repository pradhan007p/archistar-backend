<?php


namespace App\Repositories;


use App\Models\PropertyAnalytics;
use Illuminate\Support\Facades\DB;

class PropertyAnalyticRepository extends Repository
{
    /**
     * @inheritDoc
     */
    function setModel()
    {
        return (new PropertyAnalytics());
    }

    /**
     * Get all analytics for provided property ID
     *
     * @param  string  $propertyId
     * @param  int  $limit
     * @return
     */
    public function getPropertyAnalytics($propertyId = '', $limit = 15)
    {
        return $this->model
            ->where('property_id', $propertyId)
            ->get();
    }

    /**
     * Create New Property Analytics
     *
     * @param $data
     * @return mixed|Object
     */
    public function createAnalytics($data)
    {
        return $this->store($data);
    }

    /**
     * Update Specific Analytics
     *
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateAnalytics($id, $data)
    {
        return $this->update($id, $data);
    }

    /**
     * Get summary of property based on provided filter type and values
     *
     * @param $type  'suburb|state|country'
     * @param $value
     *
     * @return Object
     */
    public function getAnalyticsSummary($type = '', $value = '')
    {

        $allowedFilterTypes = ['suburb', 'state', 'country'];

        if (!in_array($type, $allowedFilterTypes))
            abort(500, 'Invalid filter type');

        $response = DB::table('property_analytics as pa')
            ->join('properties', 'properties.id', '=', 'pa.property_id')
            ->whereRaw("properties.$type LIKE '%$value%'")
            ->selectRaw('MIN(pa.value) as min, MAX(pa.value) as max,  AVG(pa.value) as avg, properties.suburb, properties.state, properties.country')
            ->groupBy('properties.suburb', 'properties.state', 'properties.country')
            ->first();

        return $response;
    }
}
