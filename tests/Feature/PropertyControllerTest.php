<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PropertyControllerTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /**
     * Test Property Listing Functionality
     *
     * @return void
     */
    public function testListProperty()
    {
        $response = $this->call('GET', '/api/property/');

        $this->assertEquals(200, $response->status());
    }

    /**
     * Test Property Create Functionality
     */
    public function testCreateProperty()
    {
        $data = [
            'suburb'  => 'Parramatta',
            'state'   => 'NSW',
            'country' => 'Australia'
        ];

        $response = $this->post('/api/property/store', $data);

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals($data['suburb'], $response['data']['suburb']);
        $this->assertEquals(201, $response->status());
    }

    /**
     * Test Property Update Functionality
     */
    public function testRequestPropertyValidationOnCreate()
    {
        $data = [
            'suburb'  => '',
            'state'   => '',
            'country' => ''
        ];

        $response = $this->post('/api/property/store', $data);

        $this->assertEquals(422, $response->status());
    }
}
