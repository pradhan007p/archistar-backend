<?php

namespace Tests\Feature;

use App\Models\AnalyticType;
use App\Models\Property;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PropertyAnalyticsControllerTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /**
     * Test Creating new analytics for a property
     *
     * @return void
     */
    public function testCreateAnalytics()
    {
        $data = [
            'suburb'  => 'Parramatta',
            'state'   => 'NSW',
            'country' => 'Australia'
        ];

        $property = Property::create($data);

        $analytics = [
            'name'               => 'max_Bld_Height_m',
            'units'              => 'm',
            'is_numeric'         => '1',
            'num_decimal_places' => '1'
        ];

        $analytic = AnalyticType::create($analytics);

        $data = [
            'property_id'      => $property->id,
            'analytic_type_id' => $analytic->id,
            'value'            => '3'
        ];

        $response = $this->post('/api/analytics/store', $data);

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(201, $response->status());
    }

    /**
     * Test Property Analytics summary based on filter type
     */
    public function testAnalyticsSummary()
    {
        $response = $this->call('GET', '/api/analytics/summary/suburb/Parramatta');

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(200, $response->status());
    }
}
