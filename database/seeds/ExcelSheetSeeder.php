<?php

use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class ExcelSheetSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->file = '/database/seeds/BackEndTest_TestData_v1.1.xlsx';

        //Mapping the excel sheets tab with table
        $this->worksheetTableMapping = [
            'Properties'         => 'properties',
            'AnalyticTypes'      => 'analytic_types',
            'Property_analytics' => 'property_analytics'
        ];

        //Mapping of Excel sheet header with Table Columns
        $this->aliases = [
            'Property Id'     => 'id',
            'Suburb'          => 'suburb',
            'State'           => 'state',
            'Counrty'         => 'country',
            'anaytic_type_id' => 'analytic_type_id',
            'Guid'            => 'guid'
        ];

        $this->defaults = [
            'Guid' => 'seed'
        ];
        //Needs to be disable this to be prevent previous records from deleting
        $this->truncate = false;

        parent::run();

    }
}
