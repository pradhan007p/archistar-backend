<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_analytics', function (Blueprint $table) {
            $table->bigIncrements('id'); //Primary Key
            $table->bigInteger('property_id')->unsigned();
            $table->bigInteger('analytic_type_id')->unsigned()->nullable();
            $table->string('value')
                ->nullable();

            $table->timestamps();

            $table->unique(['property_id', 'analytic_type_id']);

            //Foreign Key Mapping
            $table->foreign('property_id')->references('id')
                ->on('properties')
                ->onDelete('cascade');

            $table->foreign('analytic_type_id')->references('id')
                ->on('analytic_types')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_analytics');
    }
}
