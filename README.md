# Setting up the Project

- Run `composer install` to install required dependencies
- Copy .env.example file to .env
- Update Database credentials on .env file
- Generate APP Key using the command `php artisan key:generate`
- Run `php artisan migrate` to run the migration script
- Run `php artisan serve` to serve the laravel project
- Run `php artisan route:list` to view all the API Routes
- Run `./vendor/phpunit/phpunit/phpunit` to run the tests


## Available API Routes:
```
| POST     | api/analytics/store 
| GET|HEAD | api/analytics/summary/{type}/{value}
| PATCH    | api/analytics/update/{id} 
| GET|HEAD | api/analytics/{property_id}
| GET|HEAD | api/property 
| POST     | api/property/store 
| PATCH    | api/property/update/{id} 
| GET|HEAD | api/property/{id} 
```
