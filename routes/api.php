<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Property API Routes */
Route::group(['prefix' => 'property', 'namespace' => 'Api'], function () {
    Route::get('/', 'PropertyController@index');
    Route::post('/store', 'PropertyController@store');
    Route::get('/{id}', 'PropertyController@show');
    Route::patch('/update/{id}', 'PropertyController@update');
});

Route::group(['prefix' => 'analytics', 'namespace' => 'Api'], function () {
    Route::get('/{property_id}', 'PropertyAnalyticsController@index');
    Route::post('/store', 'PropertyAnalyticsController@store');
    Route::patch('/update/{id}', 'PropertyAnalyticsController@update');

    Route::get('/summary/{type}/{value}', 'PropertyAnalyticsController@propertyAnalyticsSummary');
});

